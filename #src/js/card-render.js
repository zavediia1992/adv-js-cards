function renderCard(key, cardKey) {


    let p = document.createElement('p');
    let textInput = document.createElement('input');
    textInput.classList.add("input-data");
    textInput.readOnly = true;
    textInput.setAttribute("type", "text", );
    switch (key) {
        case 'patient':
            p.innerHTML = 'Фио';
            textInput.value = cardKey;
            p.classList.add('patient');
            break;
        case 'doctors':
            switch (cardKey) {
                case 'dentist':
                    textInput.value = 'Стоматолог';
                    break;
                case 'cardiologist':
                    textInput.value = 'Кардиолог';
                    break;
                case 'therapist':
                    textInput.value = 'Терапевт';
                    break;
            }
            p.innerHTML = 'Доктор';
            p.classList.add('doctor-card');
            break;

        case 'description':
            p.innerHTML = 'Краткое описание';
            textInput.value = cardKey;
            textInput.classList.add("input-data__desc");
            break;

        case 'urgency':
            switch (cardKey) {
                case 'prioritised':
                    textInput.value = 'Приоритетная';
                    break;
                case 'regular':
                    textInput.value = 'Обычная';
                    break;
                case 'urgent':
                    textInput.value = 'Срочная';
                    break;
            }
            p.innerHTML = 'Срочность';
            textInput.classList.add("input-data__urgency");
            break;
        case 'pressure':
            p.innerHTML = 'Обычное давление';
            textInput.value = cardKey;
            break;
        case 'weight-index':
            p.innerHTML = 'Индекс массы';
            textInput.value = cardKey;
            break;
        case 'sickness':
            p.innerHTML = 'Перенесенные сердечные заболевания сердечно-сосудистой системы';
            textInput.value = cardKey;
            break;
        case 'age':
            p.innerHTML = 'Возраст';
            textInput.value = cardKey;
            break;
        case 'last-visit':
            p.innerHTML = 'Дата последнего посещения';
            textInput.value = cardKey;
            break;
        case 'goal':
            p.innerHTML = 'Цель Визита';
            textInput.value = cardKey;
            textInput.classList.add("input-data__goal");
            break;
        case 'id':
            cardForm.setAttribute('data-card-id' , cardKey);
            cardId.push(cardKey);
            removeCard(cardId);
            break;
    }

    if (p.className === 'patient' || p.className === 'doctor-card'){
        cardDataMain.appendChild(p);
        p.appendChild(textInput);
    }
    else if (textInput.value !=='') {
        cardDataMore.appendChild(p);
        p.appendChild(textInput);
    }
    let inpData = document.querySelector('.input-data');
    inpData.readOnly = true;

    cardMore();

}
