function getSearchInput(textInput, status, rank) {
        textInput = document.querySelector('.search-items__item-input');
        status = document.querySelector('#status');
        rank = document.querySelector('#rank');
changeInput(textInput, status, rank)
}
function changeInput(textInputChange, statusChange, rankChange) {
    textInputChange.oninput = function (){
        getSearchInput(textInputChange,statusChange, rankChange);
    }
    statusChange.onchange = function () {
        getSearchInput(textInputChange,statusChange, rankChange)
    }
    rankChange.onchange = function () {
        getSearchInput(textInputChange,statusChange, rankChange)
    }
    console.log(textInputChange.value, statusChange.value, rankChange.value);
    getCards(textInputChange, statusChange, rankChange);
}
getSearchInput();

function getCards(textInputChange, statusChange, rankChange) {
    let callCards = document.querySelectorAll('.card');
    let goal = document.querySelectorAll('.input-data__goal');
    let desc = document.querySelectorAll('.input-data__desc');
    let rank = document.querySelectorAll('.input-data__urgency');
    let rankCheck;
    console.log('get Cards');
    switch (rankChange.value) {
        case 'prioritised':
            rankCheck = 'Приоритетная';
            break;
        case 'regular':
            rankCheck = 'Обычная';
            break;
        case 'urgent':
            rankCheck = 'Срочная';
            break;
    }
    console.log(rankCheck);
    
    for(let i = 0; i < callCards.length; i++) {
        if (!(goal[i].value.includes(textInputChange.value) || desc[i].value.includes(textInputChange.value)) ) {
                callCards[i].style.display = 'none';
        }

        else if (!(rank[i].value === rankCheck) && rankCheck !== undefined) {
            callCards[i].style.display = 'none';
        }

        else {
            callCards[i].style.display = 'block';
        }
        }
    }


