let visitList = document.querySelector('.visit-list');
let cardForm;
let doctor;
let patient;
let cardDataMain;
let cardDataMore;
let cardId = [];
function getCardData(card) {
    cardForm = document.createElement('form');
    cardDataMore = document.createElement('div');
    cardDataMore.classList.add('card-data__more');
    cardDataMore.style.display = 'none';
    let btnChange = document.createElement('button');
    btnChange.classList.add('card-button__change');
    btnChange.innerText = 'Редактировать';
    btnChange.type = 'button';
    cardForm.classList.add('card');
    cardForm.setAttribute('id', 'card');
    visitList.append(cardForm);
    let btn = document.createElement('button');
    btn.classList.add('card-button');
    btn.innerText = 'Показать больше';
    btn.type = 'button';
    let rmBtn = document.createElement('button');
    rmBtn.innerText = 'X';
    rmBtn.type = 'button';
    rmBtn.classList.add('remove-card-button');
    cardDataMain = document.createElement('div');
    cardDataMain.classList.add('card-data__main');
    cardForm.appendChild(rmBtn);
    cardForm.appendChild(cardDataMain);
    cardForm.appendChild(cardDataMore);
    cardForm.appendChild(btn);
    console.log('this is card from get card data' + card);
    Object.keys(card).forEach(key => {
        console.log("this is card Checker", key, card[key]);
        renderCard(key, card[key]);
    })
    cardDataMore.append(btnChange);
}

function cardMore() {
    let cardButton = document.querySelectorAll('.card-button');
    let cardMoreData = document.querySelectorAll('.card-data__more');
    for (let i = 0; i < cardButton.length; i++) {
            cardButton[i].onclick = function (e) {
                if(e.target) {
                    cardMoreData[i].style.display = 'block';
                    cardButton[i].style.display = 'none';
                    changeButton();
                    e.preventDefault();
                }
            }
    }
}
let cardArray = [];
function removeCard(id) {
    let cards = document.querySelectorAll('.card');
    let removeCardBtn = document.querySelectorAll('.remove-card-button')
    for (let i = 0; i < removeCardBtn.length; i++) {

        removeCardBtn[i].onclick = function (e) {
            if(e.target){
                deleteCard(id[i]);
                cards[i].remove();
                console.log(cards.length);
                noData();
            }
        }
        }
    console.log(cardArray);
    }
function changeButton() {
    let changeBtn = document.querySelectorAll('.card-button__change');
    let visit = document.querySelector('.visit-modal');
        for (let i = 0; i < changeBtn.length; i++) {
            changeBtn[i].onclick = function (e) {
                let header = document.querySelector('.visit-modal__container_h3');
                if(e.target) {
                    visit.style.display = 'block';
                    e.preventDefault();
                    header.innerHTML = "Change Visit";
                    changeCard(i);
                    fillModal(i);
                }
            }
    }
}
function noData() {
        let msg = document.querySelector('.message');
        let cards = document.querySelectorAll('.card');
        let cardLength = cards.length;
        switch (cardLength) {
            case 'null':
                msg.style.display = 'block';
                break;
            case 0:
                msg.style.display = 'block';
                break;
            case 'undefined':
                msg.style.display = 'block';
                break;
            default:
                msg.style.display = 'none';
                break;
            }
    }

function checkClickedNotInsideCard() {
    document.addEventListener("click", (evt) => {
        const flyoutElement = document.querySelectorAll(".card");
        let cardButton = document.querySelectorAll('.card-button');
           flyoutElement.forEach(element => {
                if(evt.target.closest('.card')) {
                    return;
                }
                let cmd = document.querySelectorAll('.card-data__more');
                for (let i = 0; i < cardButton.length; i++) {
                    cmd[i].style.display = "none";
                    cardButton[i].style.display = "block";

               }

               });

           }, false);
}
checkClickedNotInsideCard();


function changeCard(i) {
        const card = document.querySelectorAll('#card');
        let cardId = card[i].getAttribute('data-card-id');

        console.log(cardId);
}

function fillModal(i) {
    let doctor = document.querySelectorAll('.doctor-card .input-data');
    let docData = doctor[i].value;


    switch (docData){
        case 'Стоматолог':
            let docs = document.getElementById('visit-doctor');
            docs.value = 'dentist';


                selectDocContent(docs.value);


            console.log("dentist is her")


            console.log("selectDocContent(option)");
            break;
    }
    console.log(docData);
}




