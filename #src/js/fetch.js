let a;
function getIt() {
    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        }}
    )
        .then(response => {
            if (!response.ok) {
                throw new Error("HTTP error " + response.status);
                let visitCreate = document.querySelector('.visit-create');
                visitCreate.style.display = 'none';
                let loginBtn = document.querySelector('btn-login');
                loginBtn.style.display = 'block';
            }
            return response.json();
        })
        .then(
            data => {
              //  let visitList = document.querySelector('.visit-list');
                let item = JSON.stringify(data);
                console.log(data.length);
                let object = JSON.parse(item);
                for (let i = 0; i < object.length; i++){
                    getCardData(object[i]);
                }
                console.log('data')
                noData();
            })
}

function deleteCard(cardId) {
    fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${accessToken}`
        },
    })
}


