

function createCard(map) {
    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        },
        body: JSON.stringify(Object.fromEntries(map))
    })
    console.log('this is createCard map')
}
 function getCardDataInput(map) {
     map = new Map();
     let inputs = document.querySelectorAll("[data-input]");

     inputs.forEach(item => {
        let className = item.className;
        let inputValue = item.value;
         if(inputValue !== '') {
             map.set(className, inputValue)
         }
     })
     console.log('getcarddatainput' + map)
     createCard(map);
     btnClick();
     noData();
 }

//let option = this.options[this.selectedIndex].getAttribute("data-option-name");

function createCardBtn() {
    let createBtn = document.querySelector('.create');
    createBtn.addEventListener('click', function (event){
        let visitModal = document.querySelector('.visit-modal');
        visitModal.style.display = 'none';
        event.preventDefault();
        getCardDataInput();
    })
}

function closeCreateModal() {
    const close = document.querySelector('.close');

    close.addEventListener('click', function (event){
        const createCard = document.querySelector('.visit-modal');
        createCard.style.display = 'none';
        event.preventDefault();
    })

}
createCardBtn();